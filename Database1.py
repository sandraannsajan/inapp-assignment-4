
import sqlite3
conn = sqlite3.connect('cars.db')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS CARS")
query1 = """CREATE TABLE CARS(
        CAR_NAME CHAR(20) NOT NULL,
        OWNER_NAME CHAR(20) NOT NULL)"""
cursor.execute(query1)
query2 = ('INSERT INTO CARS(CAR_NAME, OWNER_NAME) '
         'VALUES (:CAR_NAME, :OWNER_NAME);')

for i in range(10):
    print(i+1)
    car=input("Enter Car Name: ")
    name=input("Enter Owner Name: ")
    print("\n")
    params = {
            'CAR_NAME': car,
            'OWNER_NAME': name
        }
    cursor.execute(query2, params)
output=cursor.execute("SELECT * from CARS")
print(output.fetchall())
conn.commit()
conn.close()
