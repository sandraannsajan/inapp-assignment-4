import sqlite3
conn = sqlite3.connect('Medical.db')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS HOSPITAL")
query1 = """CREATE TABLE HOSPITAL(
        HOSPITAL_ID INT PRIMARY KEY NOT NULL, 
        HOSPITAL_NAME CHAR(20) NOT NULL, 
        BED_COUNT CHAR(20) NOT NULL)"""
cursor.execute(query1)

conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT) "
             "VALUES (1, 'MAYO CLINIC', 200)")
conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT) "
             "VALUES (2, 'CLEVELAND CLINIC', 400)")
conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT)"
             "VALUES (3, 'JOHNS HOPKINS', 1000)")
conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT)"
             "VALUES (4, 'UCLA MEDICAL CENTER', 1500)")

cursor.execute("DROP TABLE IF EXISTS DOCTOR")
query2 = """CREATE TABLE DOCTOR(
        DOCTOR_ID INT PRIMARY KEY NOT NULL,
        DOCTOR_NAME CHAR(20) NOT NULL,
        HOSPITAL_ID INT NOT NULL,
        JOINING_DATE DATE NOT NULL,
        SPECIALITY CHAR(20) NOT NULL,
        SALARY INT NOT NULL,
        EXPERIENCE CHAR(20))"""
cursor.execute(query2)

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (101, 'David', 1, '2005-02-10', 'Pediatric', 40000, 'NULL')")
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (102, 'Michael', 1, '2018-07-23', 'Oncologist', 20000, 'NULL')")
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (103, 'Susan', 2, '2016-05-19', 'Garnacologist', 25000, 'NULL')")
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (104, 'Robert', 2, '2017-12-28', 'Pediatric', 28000, 'NULL')")
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (105, 'Linda', 3, '2004-06-04', 'Garnacologist', 42000, 'NULL')")
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (106, 'William', 3, '2012-09-11', 'Dermatologist', 30000, 'NULL')")
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (107, 'Richard', 4, '2014-08-21', 'Garnacologist', 32000, 'NULL')")
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE) "
             "VALUES (108, 'Karen', 4, '2011-10-17', 'Radiologist', 30000, 'NULL')")

a='y'
while(a=='y'):
    spe = input('\nEnter the speciality of doctor:')
    sal = int(input('Enter the salary of doctor:'))
    output1 = conn.execute("SELECT DOCTOR_NAME from DOCTOR where SPECIALITY='{}' and SALARY>={}".format(spe,sal))
    print(output1.fetchall())
    print("\n")
    a=input("Do you want to keep checking? (y/n)")

a='y'
while(a=='y'):
    hos=int(input('\nEnter the hospital_id:'))
    print("Doctor names:")
    output2 = conn.execute("SELECT DOCTOR_NAME from DOCTOR where HOSPITAL_ID={}".format(hos))
    print(output2.fetchall())
    print("Hospital name:")
    output3 = conn.execute("SELECT HOSPITAL_NAME from HOSPITAL where HOSPITAL_ID={}".format(hos))
    print(output3.fetchall())
    a=input("Do you want to keep checking? (y/n)")

conn.commit()
conn.close()
